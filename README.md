<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [Arista - EOS - IAG](https://gitlab.com/itentialopensource/pre-built-automations/arista-eos-iag)

<!-- Update the below line with your Pre-Built name -->
# Arista EOS Upgrade IAG

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->The Arista EOS Upgrade IAG Pre-built enables Network Engineers to save hours of manual CLI entry with an automation that executes the standard procedure to upgrade or downgrade network switches. This Pre-built contains the Arista EOS Upgrade - IAG workflow using IAP. It is supported by the Device Connection Health Check and Command Template Runner workflows. 
The Arista EOS Upgrade - IAG workflow requires that the desired binary *.swi image file is already downloaded locally on the destination device's bootflash. The file integrity can be verified at the terminal using md5 before launching this Pre-built or selected as an optional verification step within the Arista EOS Upgrade input form (operator must input expected checksum, available from Arista EOS download page). 
At the end of the workflow, the user will be presented with a comprehensive report that lists the pre/post-checks differences.
This solution is a programmtic execution of the procdure described on Arista's website [here](https://www.arista.com/en/um-eos/eos-standard-upgrades-and-downgrades#modify_boot_config_for_single_supervisor_switch) and consists of the following:
<details><summary>Click to expand</summary>

* Main - Workflow (Arista EOS Upgrade - IAG)
* Performs device prechecks. 
* Verifies device is on a different version than the requested one. 
* Verifies the desired file is present in device flash directory.
* Optional verification of file integrity using md5 checksum.
* Performs device commands.
	* Backup the running-config locally on flash drive dynamically naming it by job_ID.
	* Saves interfaces status to job runtime log.
	* Performs command to update the boot-config file to point to the new EOS image.
	* Performs the write command to direct the switch to load the newer image version upon the next reboot.
	* Issues the reload command to the device.
* Waits for device to become available after reboot.
* Confirms reliable connectivity via ping consistency with the *Device Health Checks* child workflow.
* Performs postchecks to verify the device functionality running the new version.
* Saves the new running-configurations to job runtime log and locally on flash drive dynamically name if by job_ID.
* Shows a Pre-Post-Checks difference report.
* Performs a back-out, if requested and returns the Start-Up Boot-Config file to the precheck state.
* Error handling to Terminate —> Retry or Skip —> Retry
		
- Command Template Runner child jobs will run the commands against the switch for:	
	* Prechecks
	* Preparation
	* Upgrade
	* Write
	* Verification 	
	* Reload
	* Postchecks		
	
- Operations Manager Automation with a JSON-Form:	
	* Select Zero-Touch (unselected runs automation in verbose mode automatically).
	* Requires user to select the destination device to run the automation against.
	* Requires user to select software version to point the boot-configuration to (file names are hard coded in JSON form).
	* Allows user the option to include, or not, an md5 checksum integrity validation of the image file.
	* Requires user to select Ping delay, Ping retries, and Ping consistency variables for the Device Health Check child job.
</details>
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 5 m 34s

## Installation Prerequisites
Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2023.1`

## Requirements

This Pre-built requires the following:
* Arista EOS Single-Supervisor Switch (not compatible with switches participating in an MLAG)
* Itential Automation Gateway (IAG)

<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->

## Features

The main benefits and features of the Pre-Built are outlined below.

<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
* Saves hours of manual CLI entry.
* Best use case is when accelerated upgrade is not an option, like software downgrades.
* This automation follows a procedure that will minimize packet loss.
* Automatically checks if device is compatible with workflow.
* Optional verification of file integrity using md5 checksum.
* Verifies file presence on switch.
* Uses dynamic naming conventions for backup running-config files
* Verbose mode asks for operator confirmation prior to pushing config changes to the device.
* Allows operator to backout of changes and return device boot-config to precheck state.
* Error handling allows retry or skip.

<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## Future Enhancements

<!-- OPTIONAL - Mention if the Pre-Built will be enhanced with additional features on the road map -->
<!-- Ex.: This Pre-Built would support Cisco XR and F5 devices -->
* Future enhancements would support dual-supervisor switches and NSO managed devices.


## How to Install

To install the Pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-built. 
* The Pre-built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-built:

* The main entry point for this Pre-built is Operations Manager.

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
